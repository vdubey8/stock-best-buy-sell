import sys

# INPUT FORMAT [(day, stock price), ...]
# OUTPUT (buy day, sell day, profit)

data = [(5, 80), (10, 120), (15, 90), (20, 130), (30, 110)]

minPrice = sys.maxint
maxProfit = 0
buy = 0
sell = 0

for i in range(0, len(data)):

    if minPrice > data[i][1]:
        buy = data[i][0]
        minPrice = data[i][1]

    if maxProfit < data[i][1]-minPrice:
        sell = data[i][0]
        maxProfit = data[i][1]-minPrice


print (buy, sell, maxProfit)